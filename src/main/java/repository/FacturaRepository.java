package repository;

import java.sql.SQLException;

import dao.FacturaDAO;
import modelo.ClienteBasicDTO;
import modelo.FacturaFullDTO;

public class FacturaRepository {
	FacturaDAO facturaDao;

	public FacturaRepository() throws SQLException {		
		facturaDao = new FacturaDAO();
	}

	public FacturaFullDTO get(int id) throws SQLException {
		return null;
	}
	
	public boolean exists(int id) throws SQLException {		
		return false;
	}
	
	public FacturaFullDTO save(FacturaFullDTO factura) throws SQLException {		
		return null;
	}

	public boolean delete(FacturaFullDTO factura) throws SQLException {
		return false;
	}

	public void loadLinFacturas(FacturaFullDTO factura) throws SQLException {
		
	}
	

	public void close() throws SQLException {
		facturaDao.close();
	}
}
