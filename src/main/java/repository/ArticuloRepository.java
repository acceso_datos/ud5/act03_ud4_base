package repository;

import java.sql.SQLException;
import java.util.List;

import dao.ArticuloDAO;
import dao.GrupoDAO;
import modelo.ArticuloBasicDTO;
import modelo.ArticuloFullDTO;
import modelo.GrupoBasicDTO;

public class ArticuloRepository {
	ArticuloDAO articuloDao;

	public ArticuloRepository() throws SQLException {
		articuloDao = new ArticuloDAO();
	}

	public ArticuloBasicDTO get(int id) throws SQLException {
		return null;
	}
	
	public ArticuloFullDTO getFull(int id) throws SQLException {
		return null;
	}
	
	public List<ArticuloBasicDTO> getAll() throws SQLException {
		return null;
	}
	
	public List<ArticuloFullDTO> getAllFull() throws SQLException {
		return null;
	}

	public boolean exists(int id) throws SQLException {
		return false;
	}

	public ArticuloFullDTO save(ArticuloFullDTO articulo) throws SQLException {
		return null;
	}

	public boolean delete(ArticuloFullDTO articulo) throws SQLException {
		return false;
	}

	public List<ArticuloBasicDTO> getByName(String nombre) throws SQLException {
		return null;
	}
	
	public List<ArticuloFullDTO> getByNameFull(String nombre) throws SQLException {
		return null;
	}	
	
	public List<ArticuloBasicDTO> getByNamePrice(String nombre, float precio) throws SQLException {
		return null;
	}
	
	public List<ArticuloFullDTO> getByNamePriceFull(String nombre, float precio) throws SQLException {
		return null;
	}
	
	public List<ArticuloBasicDTO> getByGroup(int grupo) throws SQLException {
		return null;
	}
	
	public List<ArticuloFullDTO> getByGroupFull(int grupo) throws SQLException {
		return null;
	}
		
	
	public void close() throws SQLException {
		articuloDao.close();
	}
}
