package repository;

import java.sql.SQLException;
import java.util.List;

import dao.ClienteDAO;
import dao.FacturaDAO;
import modelo.ClienteBasicDTO;
import modelo.FacturaBasicDTO;

public class ClienteRepository {
	ClienteDAO clienteDao;
	FacturaDAO facturaDao;

	public ClienteRepository() throws SQLException {
		clienteDao = new ClienteDAO();
		facturaDao = new FacturaDAO();
	}

	public ClienteBasicDTO get(int id) throws SQLException {
		return clienteDao.find(id);
	}

	public ClienteBasicDTO getByUsername(String userName) throws SQLException {
		return clienteDao.findByUsername(userName);
	}

	public boolean exists(int id) throws SQLException {
		if (clienteDao.find(id) != null) {
			return true;
		}
		return false;
	}

	public boolean existsByUsername(String userName) throws SQLException {
		if (clienteDao.findByUsername(userName) != null) {
			return true;
		}
		return false;
	}

	public ClienteBasicDTO save(ClienteBasicDTO cliente) throws SQLException {
		if (this.exists(cliente.getId())) {
			clienteDao.update(cliente);
		} else {
			clienteDao.insert(cliente);
		}
		return cliente;
	}

	public boolean delete(ClienteBasicDTO cliente) throws SQLException {
		return clienteDao.delete(cliente);
	}

	public List<ClienteBasicDTO> getByName(String nombre) throws SQLException {
		ClienteBasicDTO muestra = new ClienteBasicDTO(nombre, null);
		return clienteDao.findByExample(muestra);
	}

	public void loadFacturas(ClienteBasicDTO cliente) throws SQLException {
		
	}

	public float calculaTotalFacturado(int clienteId) throws SQLException {
		float totalFacturado = 0;
		
		// Opción más costosa a nivel de recursos
		ClienteBasicDTO cliente = get(clienteId);
		loadFacturas(cliente);
		List<FacturaBasicDTO> facturas = cliente.getFacturas();
		if (facturas != null) {			
			for (FacturaBasicDTO f : facturas) {
				totalFacturado += facturaDao.getImporteTotal(f.getId());
			}			
		}
		
		// Opción más óptima: llamada a método específico del dao
		// totalFacturado = facturaDao.getTotalFacturadoByCliente(clienteId);
		
		return totalFacturado;
	}

	public void close() throws SQLException {
		clienteDao.close();
	}
}
