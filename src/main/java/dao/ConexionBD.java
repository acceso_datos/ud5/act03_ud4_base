package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

// Patrón singleton
public class ConexionBD {

    private static final String JDBC_URL = "jdbc:mariadb://192.168.56.101/empresa_ad";
//    private static final String JDBC_URL = "jdbc:postgresql://192.168.56.102:5432/batoi?currentSchema=empresa_ad";
    private static Connection con = null;    

    public static Connection getConnection() throws SQLException {
        if (con == null) {
            Properties pc = new Properties();
            pc.put("user", "batoi");
            pc.put("password", "1234");
            con = DriverManager.getConnection(JDBC_URL, pc);
        }
        return con;
    }

    public static void close() throws SQLException {
        if (con != null) {
            con.close();
            con = null;
        }
    }
    
    public static void beginTransaction() throws SQLException {
    	con.setAutoCommit(false);
    }
    
    public static void commit() throws SQLException {
    	con.commit();
    	con.setAutoCommit(true);
    }
    
    public static void rollback() throws SQLException {
    	con.rollback();
    	con.setAutoCommit(true);
    }

}
