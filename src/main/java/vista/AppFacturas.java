package vista;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import modelo.ClienteBasicDTO;
import modelo.FacturaFullDTO;
import modelo.LineaFacturaBasicDTO;
import modelo.VendedorBasicDTO;
import service.ClienteService;
import service.FacturaService;

public class AppFacturas {
	static FacturaService facturaServicio = null;
	static ClienteService clienteServicio = null;

	public static void main(String[] args) {
		casosUsoFacturas();

	}

	private static void casosUsoFacturas() {
		try {
			facturaServicio = new FacturaService();
			clienteServicio = new ClienteService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (facturaServicio != null) {
			// casos
			ejemploObtenerFacturas(); 	// Lista todas las facturas página a página.
			ejemploObtenerFactura(); 	// Obtiene todos los datos de una factura a partir de un número.

			ejemploNuevaFactura(); 		// cabecera y lineas (pueden estar), actualización stock y transaccional.
			ejemploModificarFactura(); 	// la modificación puede implicar modificar cabecera y/o lineas. Actualización stock si es el caso. Transaccional.
			ejemploEliminaFactura(); 	// elimina factura y sus lineas. Transaccional.

		}
	}

	private static void ejemploEliminaFactura() {
		try {
			System.out.println("Eliminando factura...");
			FacturaFullDTO factura = facturaServicio.obtenerFactura(5020, true);    // true: indica que cargue las lineas también
			facturaServicio.eliminaFactura(factura);
			System.out.println("Factura eliminada correctamente");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	private static void ejemploModificarFactura() {		
		ClienteBasicDTO cliente;
		try {
			System.out.println("Modificando factura...");
			FacturaFullDTO factura = facturaServicio.obtenerFactura(5015, true); 	// true: indica que cargue las lineas también
			
			cliente = clienteServicio.obtenerCliente(2);
			factura.setCliente(cliente);
			factura.setFormaPago("Otra");
			
			List<LineaFacturaBasicDTO> lineas = factura.getLineas();			
			lineas.remove(0);
			lineas.add(new LineaFacturaBasicDTO(0, 0, 5, 10));
			LineaFacturaBasicDTO lin = lineas.get(0);
			lin.setCantidad(50);
			
			facturaServicio.guardaFactura(factura);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	private static void ejemploNuevaFactura() {
		ClienteBasicDTO cliente;
		try {
			System.out.println("Creando nueva factura");
			cliente = clienteServicio.obtenerCliente(1);
			VendedorBasicDTO vendedor = new VendedorBasicDTO(1, null, null, 0f);
			FacturaFullDTO factura = new FacturaFullDTO(LocalDate.now(), cliente, vendedor, "tarjeta");
			List<LineaFacturaBasicDTO> lineas = new ArrayList<LineaFacturaBasicDTO>();
			lineas.add(new LineaFacturaBasicDTO(0, 0, 1, 10));
			lineas.add(new LineaFacturaBasicDTO(0, 0, 10, 20));
			lineas.add(new LineaFacturaBasicDTO(0, 0, 2, 12));
			factura.setLineas(lineas);
			facturaServicio.guardaFactura(factura);
			System.out.println("Factura insertada correctamente: " + factura);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploObtenerFactura() {
		FacturaFullDTO factura;
		try {
			factura = facturaServicio.obtenerFactura(4, true);
			System.out.println(factura);
			if (factura.getLineas() != null) {
				for (LineaFacturaBasicDTO lin : factura.getLineas()) {
					System.out.println("\t" + lin);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploObtenerFacturas() {
		try {
			List<FacturaFullDTO> lista;
			for (int i = 1; i <= facturaServicio.obtenerNumPaginas(); i++) {
				System.out.println("***** Página " + i + " *****");
				lista = facturaServicio.obtenerFacturas(i, true);
				for (FacturaFullDTO factura : lista) {
					System.out.println(factura);
					if (factura.getLineas() != null) {
						for (LineaFacturaBasicDTO lin : factura.getLineas()) {
							System.out.println("\t" + lin);
						}
					}
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

}
