package vista;

import java.util.List;

import modelo.ArticuloFullDTO;
import modelo.GrupoBasicDTO;
import service.ArticuloService;

public class AppArticulos {
	static ArticuloService articuloServicio = null;

	public static void main(String[] args) {
		casosUsoArticulos();

	}

	private static void casosUsoArticulos() {
		try {
			articuloServicio = new ArticuloService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (articuloServicio != null) {
			ejemploObtenerInfoArticulo();
			ejemploObtenerTodosArticulos();

			ejemploNuevoArticulo();
			ejemploModificaArticulo();
			ejemploActualizaStock();
			ejemploEliminaArticulo();

			ejemploBusquedaPorNombre();
			ejemploBusquedaPorNombreHastaPrecio();
			ejemploObtenerArticulosGrupo();
			ejemploBusquedaPorNombreEntrePrecios();
		}
	}

	private static void ejemploEliminaArticulo() {
		ArticuloFullDTO articulo;
		try {
			articulo = articuloServicio.obtenerArticulo(200);
			articuloServicio.eliminaArticulo(articulo);
			System.out.println("Articulo eliminado correctamente");
		} catch (Exception e) {
			System.out.println("Imposible eliminar articulo: " + e.getMessage());
		}

	}

	private static void ejemploActualizaStock() {
		try {
			articuloServicio.actualizaStock(1, 12);
			System.out.println("Stock articulo actualizado correctamente");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploObtenerInfoArticulo() {
		ArticuloFullDTO articulo;
		try {
			articulo = articuloServicio.obtenerArticulo(1);
			System.out.println(articulo);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploObtenerTodosArticulos() {
		System.out.println("Listado de todos los artículos (info completa)");
		try {
			List<ArticuloFullDTO> articulos = articuloServicio.obtenerArticulos();
			for (ArticuloFullDTO a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploNuevoArticulo() {
		ArticuloFullDTO nuevoArt = new ArticuloFullDTO("Impresora", 100f, "impX", 10, new GrupoBasicDTO(1, null));
		try {
			articuloServicio.altaArticulo(nuevoArt);
			System.out.println("Alta correcta: " + nuevoArt);
		} catch (Exception e) {
			System.out.println("Error en alta de articulo: " + e.getMessage());
		}
	}

	private static void ejemploModificaArticulo() {
		try {
			ArticuloFullDTO articulo = articuloServicio.obtenerArticulo(1);
			articulo.setPrecio(articulo.getPrecio() * 1.15f);
			articuloServicio.modificaArticulo(articulo);
			System.out.println("Modificación satisfactoria");
		} catch (Exception e) {
			System.out.println("Error modificando articulo: " + e.getMessage());
		}
	}

	private static void ejemploBusquedaPorNombreHastaPrecio() {
		System.out.println("Listado de monitores con precio hasta 200€");
		try {
			List<ArticuloFullDTO> articulos = articuloServicio.buscaPorNombreHastaPrecio("monitor", 200f);
			for (ArticuloFullDTO a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploObtenerArticulosGrupo() {
		System.out.println("Listado de articulos del grupo 2");
		try {
			List<ArticuloFullDTO> articulos = articuloServicio.buscaPorGrupo(2);
			for (ArticuloFullDTO a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	private static void ejemploBusquedaPorNombre() {
		System.out.println("Listado de monitores");
		try {
			List<ArticuloFullDTO> articulos = articuloServicio.buscaPorNombre("monitor");
			for (ArticuloFullDTO a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	private static void ejemploBusquedaPorNombreEntrePrecios() {
		System.out.println("Listado de monitores entre 100 y 200");
		try {
			List<ArticuloFullDTO> articulos = articuloServicio.buscaPorNombreEntrePrecios("monitor", 100f, 200f);
			for (ArticuloFullDTO a : articulos) {
				System.out.println(a);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
