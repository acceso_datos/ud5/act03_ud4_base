package vista;

import java.util.List;

import modelo.ClienteBasicDTO;
import modelo.FacturaBasicDTO;
import service.ClienteService;

public class AppClientes {
	static ClienteService clienteServicio = null;

	public static void main(String[] args) {
		casosUsoClientes();	

	}

	private static void casosUsoClientes() {
		try {
			clienteServicio = new ClienteService();
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}

		if (clienteServicio != null) {
			ejemploLogin();
			ejemploCambioContrasenya();
			ejemploCambioPerfil();
			ejemploRegistro();
			ejemploBusquedaPorNombre();
			ejemploMostrarTotalFacturacion();
			ejemploListarFacturas();
		}		
	}

	private static void ejemploLogin() {
		try {
			ClienteBasicDTO cli = clienteServicio.loginCliente("usr12", "1234");
			System.out.println("Autenticado " + cli);
		} catch (Exception e) {
			System.out.println("Error en autenticación: " + e.getMessage());
		}

	}

	private static void ejemploCambioContrasenya() {
		try {
			clienteServicio.cambiaContrasenya(12, "1234");
			System.out.println("Contraseña cambiada con éxito (cliente 12)");
		} catch (Exception e) {
			System.out.println("Error cambiando contraseña: " + e.getMessage());
		}

	}

	private static void ejemploCambioPerfil() {
		try {
			ClienteBasicDTO cli = clienteServicio.obtenerCliente(12);
			cli.setNombre("Mateo");
			clienteServicio.cambiaPerfil(cli);
			System.out.println("Perfil cambiado satisfactoriamente: " + cli);
		} catch (Exception e) {
			System.out.println("Error cambiando perfil: " + e.getMessage());
		}

	}

	private static void ejemploRegistro() {
		try {
			ClienteBasicDTO nuevoCliente = new ClienteBasicDTO("Aa Aaaa", "C/ Aa", "usr_e", "1234");
			clienteServicio.registraCliente(nuevoCliente);
			System.out.println("Registro nuevo usuario OK: " + nuevoCliente);
		} catch (Exception e) {
			System.out.println("Error registrando usuario: " + e.getMessage());
		}

	}

	private static void ejemploBusquedaPorNombre() {
		try {
			System.out.println("Busqueda por nombre 'm'");
			List<ClienteBasicDTO> listaClientes = clienteServicio.buscaPorNombre("m");
			for (ClienteBasicDTO c : listaClientes) {
				System.out.println(c);
			}
		} catch (Exception e) {
			System.out.println("Error en búsqueda: " + e.getMessage());
		}

	}
	
	private static void ejemploMostrarTotalFacturacion() {		
		int clienteId = 1;		
		float total;
		try {
			System.out.print("El total facturado del cliente " + clienteId + " es: ");
			total = clienteServicio.calculaTotalFacturado(clienteId);
			System.out.println(total+" €");
		} catch (Exception e) {
			System.out.println("Error calculando total facturacion: " + e.getMessage());
		}	
		
	}
	
	private static void ejemploListarFacturas() {		
		int clienteId = 1;		
		try {
			ClienteBasicDTO cliente = clienteServicio.obtenerCliente(clienteId);
			clienteServicio.cargaFacturas(cliente);
			List<FacturaBasicDTO> listaFacturas = cliente.getFacturas();
			// mostramos solo las 10 primeras
			for (int i = 0; i < 10; i++) {
				System.out.println(listaFacturas.get(i));
			}
			
		} catch (Exception e) {
			System.out.println("Error leyendo facturas: " + e.getMessage());
		}	
		
	}

}
