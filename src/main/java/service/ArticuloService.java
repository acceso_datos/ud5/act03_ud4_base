package service;

import java.sql.SQLException;
import java.util.List;

import modelo.ArticuloFullDTO;
import repository.ArticuloRepository;

/* Ejemplo de capa service del dominio de articulos */
public class ArticuloService {

	ArticuloRepository articuloRepository;

	public ArticuloService() throws SQLException {
		articuloRepository = new ArticuloRepository();
	}

	// Obtener artículo completo con los datos del grupo
	public ArticuloFullDTO obtenerArticulo(int art) throws Exception {
		return null;
	}

	// Obtener todos los artículos con los datos del grupo.
	public List<ArticuloFullDTO> obtenerArticulos() throws Exception {
		return null;
	}

	// Alta de un artículo nuevo
	public void altaArticulo(ArticuloFullDTO articulo) throws Exception {

	}

	// Modifica datos de un artículo
	public void modificaArticulo(ArticuloFullDTO articulo) throws Exception {

	}

	// Actualiza el stock de un artículo
	public void actualizaStock(int articulo, int nuevoStock) throws Exception {

	}

	// Busca articulos por aproximación de nombre
	public List<ArticuloFullDTO> buscaPorNombre(String nombre) throws Exception {
		return null;
	}

	// Busca articulos por aproximación de nombre y hasta precio
	public List<ArticuloFullDTO> buscaPorNombreHastaPrecio(String nombre, float precio) throws Exception {
		return null;
	}

	// Busca articulos por grupo
	public List<ArticuloFullDTO> buscaPorGrupo(int grupo) throws Exception {
		return null;
	}

	// Busca articulos por grupo
	public List<ArticuloFullDTO> buscaPorNombreEntrePrecios(String nombre, float precioDesde, float precioHasta) throws Exception {
		return null;
	}

}
