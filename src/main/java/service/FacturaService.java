package service;

import java.sql.SQLException;

import repository.FacturaRepository;

/* Ejemplo de capa service del dominio de facturas */
 
public class FacturaService {

	FacturaRepository facturaRepository;

	public FacturaService() throws SQLException {
		facturaRepository = new FacturaRepository();

	}
	
	// obtenerFactura: obtendrá factura a partir de id. Permitirá obtener la factura con o sin lineas.
	
	// obtenerFacturas: obtendrá las facturas de la página indicada. Permitirá obtener las facturas con o sin lineas.
	
	// cargarLineasFactura: permite cargar las lineas de una factura.
	
	// guardaFactura: insertará o actualizará una factura. Tendrá en cuenta que una factura puede tener lineas. Además, actualizará stock. Todo en modo transaccional.
	
	// eliminarFactura: eliminará factura y sus lineas asociadas. Todo en modo transaccional.

	
}
