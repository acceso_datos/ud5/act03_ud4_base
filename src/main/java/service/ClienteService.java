package service;

import java.sql.SQLException;
import java.util.List;

import dao.ClienteDAO;
import modelo.ClienteBasicDTO;
import repository.ClienteRepository;
import vista.Utilidades;

/* Ejemplo de capa service del dominio del cliente (usuario). 
 * Requiere modificar la tabla clientes añadiendo
 * los campos username y password
 * alter table clientes
		add column username varchar(20) unique,
		add column password char(40);

    update clientes 
		set username = concat("usr", id),
			password = sha1('1234');
 */
public class ClienteService {
	ClienteRepository clienteRepository;

	public ClienteService() throws SQLException {
		clienteRepository = new ClienteRepository();

	}

	// Comprobar si ya existe (username) con findByUserName
	// Si existe: abortar (exception)
	// Sino, realiza inserción
	public void registraCliente(ClienteBasicDTO cli) throws Exception {
		if (!clienteRepository.existsByUsername(cli.getUsername())) {
			cli.setPassword(Utilidades.sha1(cli.getPassword()));
			if (clienteRepository.save(cli) == null) {
				throw new Exception("Error al registrar usuario");
			}
		} else {
			throw new Exception("El username ya está cogido");
		}

	}

	// Comprobar si existe (username) con findByUserName
	// Si existe, comprueba password.
	// Sino, abortar (exception)
	// También se podría añadir: si contraseña caducada, abortar (excepcion).
	public ClienteBasicDTO loginCliente(String username, String password) throws Exception {
		ClienteBasicDTO cliente = clienteRepository.getByUsername(username);
		if (cliente != null) {
			String pw = Utilidades.sha1(password);
			if (!pw.equals(cliente.getPassword())) {
				throw new Exception("Fallo de autenticación");
			} else {
				// comprobar fecha de caducidad...
			}
		} else {
			throw new Exception("Fallo de autenticación");
		}
		return cliente;
	};

	// Eliminar cliente y ¿otros registros de tablas relacionadas?
	public void bajaCliente(ClienteBasicDTO cli) throws Exception {
		if (clienteRepository.delete(cli) == false) {
			throw new Exception("Imposible dar de baja el cliente");
		}

	}

	// Cambio de la contraseña del cliente (id o username)
	public void cambiaContrasenya(int idCliente, String password) throws Exception {
		ClienteBasicDTO cliente = clienteRepository.get(idCliente);
		if (cliente != null) {
			cliente.setPassword(Utilidades.sha1(password));
			// cliente.setFechaCaducidad(LocalDate.now()); // actualizamos la fecha
			// caducidad contraseña
			clienteRepository.save(cliente);
		} else {
			throw new Exception("El cliente especificado no existe");
		}
	}

	// Se usa para cambiar los datos del perfil del usuario: nombre, direccion,...
	public void cambiaPerfil(ClienteBasicDTO cli) throws Exception {
		if (cli.getId() > 0 && clienteRepository.exists(cli.getId())) {
			clienteRepository.save(cli);
		} else {
			throw new Exception("Ha habido un problema actualizando el perfil");
		}
	}

	// Busca clientes por aproximación de nombre
	public List<ClienteBasicDTO> buscaPorNombre(String nombre) throws Exception {
		List<ClienteBasicDTO> listaResultado = clienteRepository.getByName(nombre);
		if (listaResultado.size() == 0) {
			throw new Exception("No se han encontrado clientes con ese nombre");
		} else {
			return listaResultado;
		}
	}

	// Obtención de un cliente
	public ClienteBasicDTO obtenerCliente(int idCliente) throws Exception {
		ClienteBasicDTO cli = clienteRepository.get(idCliente);
		if (cli != null) {
			return cli;
		} else {
			throw new Exception("Ha habido un problema obteniendo cliente. Probablemente no existe");
		}
	}

	// Carga facturas del cliente (sin paginación)
	public void cargaFacturas(ClienteBasicDTO cliente) throws Exception {
		clienteRepository.loadFacturas(cliente);
	}

	public float calculaTotalFacturado(int clienteId) throws Exception {
		return clienteRepository.calculaTotalFacturado(clienteId);
	}

}
